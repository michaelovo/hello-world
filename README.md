
# Author name: Michael Arienmughare




### This file has been altered by Michael A on Git Bash
The above lines should be present when merged with master

# Groundhog
 
# Team: G-1
 
## Name of Project: Groundhog
 
### Title: “No Predictions Required”
 
#### Description:
 
To allow individuals to have a greater control of their life and time by showing them visually
how their time is spent each day. Allows users to mark in appointments, recurring events, and even time
off. They can then share events with friends and family, so everyone can be in the loop. You can even
write notes about each event, so you know if there’s something special that needs to be done.
 
##### Users: Designed as a commercial product, not open source
 
###### Use Benefits: A more controlled environment to manage one’s time.
 
####### Vision Statement:
 
FOR the average person WHO wants to keep track of their time. The Groundhog
Calendar is a Calendar application THAT helps you and those around you keep track of your life with
ease. UNLIKE most calendar apps OUR PRODUCT will allow you to have better control over how you
schedule your life.
 
######## Development Platform: Windows 10
 
######### Software Environment: Windows Forms Application, .NET, C#
 
